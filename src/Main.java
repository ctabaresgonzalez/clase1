public class Main {
    public static void main(String[] args) {
        Humano humano = new Humano("Camilo", "1037667016", "Sura");
        humano.hacerRuido();
        humano.trabajar();
        humano.hablar();

        Pinguino pinguino = new Pinguino("Rodolfo");
            pinguino.volar();

        Perro perro = new Perro("Luna", "Beagle");
            perro.lamer();
    }
}