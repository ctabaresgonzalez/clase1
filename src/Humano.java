public class Humano extends Animal implements Volar{
    private String id;
    private String eps;

    public Humano(String nombre, String id, String eps) {
        super(nombre);
        this.id = id;
        this.eps = eps;
    }

    public void trabajar() {
        System.out.println("Trabajo como animal");
    }
    @Override
    public void hacerRuido() {
        System.out.println("Bla bla bLa");
    }
    @Override
    public void comer() {
        System.out.println("Como como humano");
    }
    @Override
    public void hablar() {
        System.out.println("Holi");
    }
    @Override
    public void volar() {
        System.out.println("Vuelo en avión");
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getEps() {
        return eps;
    }
    public void setEps(String eps) {
        this.eps = eps;
    }
}
