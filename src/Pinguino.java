public class Pinguino extends Animal implements Volar{

    public Pinguino(String nombre) {
        super(nombre);
    }

    @Override
    public void hacerRuido() {
        System.out.println("Tweet, tweet");
    }
    @Override
    public void comer() {
        System.out.println("Como como pinguino");
    }
    @Override
    public void hablar() {
        System.out.println("TWEEEEET");
    }
    @Override
    public void volar() {
        System.out.println("Trato de volar");
    }
}
