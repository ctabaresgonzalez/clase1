public class Perro extends Animal implements Volar{
    private String raza;

    public Perro(String nombre, String raza) {
        super(nombre);
        this.raza = raza;
    }

    public void lamer() {
        System.out.println("Lick, lick");
    }
    @Override
    public void hacerRuido() {
        System.out.printf("Woof, woof");
    }
    @Override
    public void comer() {
        System.out.println("Como como perro");
    }
    @Override
    public void hablar() {
        System.out.println("WOOF WOOF");
    }
    @Override
    public void volar() {
        System.out.println("No puedo volar :(");
    }

    public String getRaza() {
        return raza;
    }
    public void setRaza(String raza) {
        this.raza = raza;
    }
}
